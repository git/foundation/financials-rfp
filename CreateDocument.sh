#!/bin/bash
CONFIG_BIBTEX=false
CONFIG_MAINFILE="main"
CONFIG_CLEANUP=true
CONFIG_CREATEDIFF=false
CONFIG_CREATE_GITVERSION=true

if [[ ${CONFIG_CREATE_GITVERSION} = "true" ]]; then
	git describe --tags | sed -e 's/_/ /g' > docversion.tmp || die
fi

# Need to loop this to get proper references. If using bibtex this needs to
# be part of the processing as well
for i in $(seq 1 3); do
	if [[ ${CONFIG_BIBTEX} = "true" && ${i} == "3" ]]; then
		bibtex ${CONFIG_MAINFILE}	
	fi;
	pdflatex ${CONFIG_MAINFILE}
done;

if [[ ${CONFIG_CREATEDIFF} = "true" ]]; then
	latexdiff ${CONFIG_MAINFILE}.tex.old ${CONFIG_MAINFILE}.tex > diff.tex
	pdflatex diff.tex
	pdflatex diff.tex
fi

# Clean up
if [[ ${CONFIG_CLEANUP} = "true" ]]; then
	for x in bbl aux blg log out dvi toc; do
		rm -rf ${CONFIG_MAINFILE}.${x}
	done;
	if [[ ${CONFIG_CREATE_GITVERSION} = "true" ]]; then
		rm -f docversion.tmp
	fi
fi;
